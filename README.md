# SESRES-D-22-00043
This directory contains the data that is used in the paper "Comparing grab and dredge sampling for shoreface benthos using ten years of monitoring data from the Sand Motor mega nourishment” by Wijsman J.W.M., J.A. Craeymeersch and P. Herman. 

- The file **V_VeenData.csv** contains the density data (numbers per m2) in the Van Veen Grab samples. StationNr = Sation ID of the sampling location. Year = year of sampling; TaxonName is name of the taxon 

- The file **DredgeData.csv** contains the density data (numbers per m2) in the benthic dredge samples. StationNr = Sation ID of the sampling location. Year = year of sampling; TaxonName is name of the taxon 

- The file **StationsvVeen.csv** contains the information of the sampled locations of the Van Veen Grab samples. StationNr = Sation ID of the sampling location. Date = sampling date; Year = year of sampling. The coordinates are given in lattitude and longitude.

- The file **StationsDredge.cs** contains the information of the sampled locations of the benthic dredge samples. StationNr = Sation ID of the sampling location. Date = sampling date; Year = year of sampling. The coordinates are given in lattitude and longitude.

